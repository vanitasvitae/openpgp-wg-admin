# IETF-116 OpenPGP WG Meeting

When: Wed, March 29, 1530-1700 local (Asia/Tokyo), 0600-0800 UTC

Where:

- On-site: room G316
- Online: via [meetecho](https://meetings.conf.meetecho.com/ietf116/?group=openpgp&short=openpgp&item=1)

# DRAFT Agenda:

-   Agenda Bash (chairs, 5)
-   Crypto-refresh issues (lead by dkg, 20, but as long as it takes)
    - summarize changes from draft -07 to -08 (dkg)
    - v3 PKESK structure for X448 and X25519 (Daniel Huigens)
        - [MR !257](https://gitlab.com/openpgp-wg/rfc4880bis/-/merge_requests/257): novel padding (already merged to -08)
        - [MR !266](https://gitlab.com/openpgp-wg/rfc4880bis/-/merge_requests/266): no padding, cleartext symmetric algorithms (replaced !257 in git head)
    - outstanding MRs (dkg)
        - [guidance on session key reuse (MR !228)](https://gitlab.com/openpgp-wg/rfc4880bis/-/merge_requests/228) (Falko Strenzke)

- Interoperability
    - `--profile` arguments to `sop` (dkg, 5)
    - Interop test suite update (Justus Winter, 10)
-   Re-chartering topics (timings: TBD)
    -  [PQC](https://datatracker.ietf.org/doc/draft-wussler-openpgp-pqc/) (Falko Strenzke)
    -   Automatic Forwarding (Aron Wussler)
        -   [As presented at IETF 114](https://datatracker.ietf.org/doc/slides-114-openpgp-forwarding-via-diverted-ecdh-key-exchanges/)
    - [Key Superseded](https://gitlab.com/openpgp-wg/rfc4880bis/-/merge_requests/222) (Aron Wussler)
    -   Various (Daniel Huigens)
        -   Long-term symmetric keys (which I [presented on at IETF 114](https://datatracker.ietf.org/doc/slides-114-openpgp-persistent-symmertric-keys/))
        -   Forward secrecy (possibly mostly covered by short-term keys + re-encryption using long-term symmetric keys, but might still be worth discussing separately, to see if we want a more "proper" solution like double ratcheting)
        -   [Domain separation](https://gitlab.com/openpgp-wg/rfc4880bis/-/merge_requests/214)
        -   Key verification, e.g. a better alternative to manually verifying fingerprints. This topic could include using QR codes or similar, or even something like Key Transparency to automatically verify keys (which Aron [presented on at IETF 113](https://datatracker.ietf.org/doc/slides-113-openpgp-key-transparency/)).
    - [Replacement for Designated Revoker](https://gitlab.com/openpgp-wg/rfc4880bis/-/merge_requests/18) (dkg)
    - [User ID clarifications](https://gitlab.com/openpgp-wg/rfc4880bis/-/merge_requests/23) (dkg)
    - [Attestation Signatures, a.k.a. "1PA3PC"](https://gitlab.com/openpgp-wg/rfc4880bis/-/merge_requests/60) (dkg)
    - WoT: [Trust Signatures](https://gitlab.com/openpgp-wg/rfc4880bis/-/issues/119), [Regex subpackets](https://gitlab.com/openpgp-wg/rfc4880bis/-/merge_requests/263), [Validation constraints](https://gitlab.com/openpgp-wg/rfc4880bis/-/issues/12), [Certification capable subkeys](https://gitlab.com/openpgp-wg/rfc4880bis/-/issues/123) (dkg)
    - [Stateless OpenPGP Interface ("SOP")](https://datatracker.ietf.org/doc/draft-dkg-openpgp-stateless-cli/) (dkg)
    - Reusing session keys in a specific application layer: E-mail Messages
    - [PGP/MIME guidance for v6 signatures](https://gitlab.com/openpgp-wg/rfc4880bis/-/issues/116)
