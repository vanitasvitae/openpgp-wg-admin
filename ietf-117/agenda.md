# OpenPGP @ IETF 117

- When: Friday Session II (2023-07-28 19:00-20:30 UTC, 12:00-13:30 local)
- Where: Golden Gate 7-8 (or [online via meetecho](https://meetings.conf.meetecho.com/ietf117/?group=openpgp&short=openpgp&item=1))

# Agenda

- Administrivia (chairs, 5)
  - Note well
  - Blue sheets (or equivalent)
  - Agenda bashing
- Status of the Crypto Refresh (chairs) (10 minutes)
- Rechartering Topics
  - [Automatic Forwarding for ECDH Curve25519 OpenPGP messages](https://datatracker.ietf.org/doc/draft-wussler-openpgp-forwarding/) (Aron Wussler) (10 minutes)
  - [Post-Quantum Cryptography in OpenPGP](https://datatracker.ietf.org/doc/draft-wussler-openpgp-pqc/) (Falko Strenzke) (10 minutes)
  - [Persistent Symmetric Keys in OpenPGP](https://datatracker.ietf.org/doc/draft-huigens-openpgp-persistent-symmetric-keys/) (Daniel Huigens) (10 minutes)
  - Other Rechartering Topics:
     - [Stateless OpenPGP Interface ("SOP")](https://datatracker.ietf.org/doc/draft-dkg-openpgp-stateless-cli/)
     - [Replacement for Designated Revoker](https://gitlab.com/openpgp-wg/rfc4880bis/-/merge_requests/18)
     - [Complexity with Intended Recipients](https://gitlab.com/openpgp-wg/rfc4880bis/-/issues/169)
     - [Domain separation](https://gitlab.com/openpgp-wg/rfc4880bis/-/merge_requests/214)
     - [Key Superseded](https://gitlab.com/openpgp-wg/rfc4880bis/-/merge_requests/222)
     - [Attestation Signatures, a.k.a. "1PA3PC"](https://gitlab.com/openpgp-wg/rfc4880bis/-/merge_requests/60)
     - [PGP/MIME guidance for v6 signatures](https://gitlab.com/openpgp-wg/rfc4880bis/-/issues/116)
     - WoT: [Trust Signatures](https://gitlab.com/openpgp-wg/rfc4880bis/-/issues/119), [Regex subpackets](https://gitlab.com/openpgp-wg/rfc4880bis/-/merge_requests/263), [Validation constraints](https://gitlab.com/openpgp-wg/rfc4880bis/-/issues/12), [Certification capable subkeys](https://gitlab.com/openpgp-wg/rfc4880bis/-/issues/123)
- Any Other Business
